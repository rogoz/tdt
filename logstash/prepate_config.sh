#!/bin/bash

while getopts t:j:b:p: option
do
        case "${option}"
        in
                p) path=${OPTARG};;
                t) test_name=${OPTARG};;            
                j) job_name=${OPTARG};;
                b) build_number=${OPTARG};;
                
        esac
done


cd logstash

IFS=

rm -rf new_*


for file in *.conf ; do
  echo "$file"
  while read line
  do
    eval "echo \"$line\"">>"new_$file"
    rm -rf $file
  done < $file
done

for file in *.conf ; do
    mv -v "$file" "${file#*_}"
done