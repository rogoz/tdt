package com.javacodegeeks;

import java.util.Random;

/**
 * Hello world!
 *
 */
public class App 
{
    public static Random random = new Random(System.currentTimeMillis());
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
