#!/bin/bash




for i in {1..100}
do
  NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 7 | head -n 1)
  echo $NAME
  curl -XPOST '127.0.0.1:9200/mynewindex/external?pretty' -d "
  {
    \"first_name\": \"Gigi${NAME}\",
    \"last_name\": \"Jojo${NAME}\",
    \"email_address\": \"Gigi${NAME}@example.com\"
  }"
done  