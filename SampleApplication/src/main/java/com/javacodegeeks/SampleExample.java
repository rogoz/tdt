package com.javacodegeeks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tuicu on 30/06/15.
 */
public class SampleExample {


    private List<Integer> integers = null;

    public SampleExample() {
        integers = new ArrayList<Integer>();
    }

    public void addInteger(int num) {
        integers.add(num);
    }

    public int getSize() {
        return integers.size();
    }
}
