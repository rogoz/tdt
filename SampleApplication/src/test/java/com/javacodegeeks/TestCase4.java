package com.javacodegeeks;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by tuicu on 30/06/15.
 */
public class TestCase4 {

    @Test
    public void test1() {
        SampleExample example = new SampleExample();


	int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test2() {
        SampleExample example = new SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test3() {
        SampleExample example = new SampleExample();


        int nr = App.random.nextInt(5);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test4() {
        SampleExample example = new SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test5() {
        SampleExample example = new SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test6() {
        SampleExample example = new SampleExample();


        int nr = App.random.nextInt(9);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test7() {
        SampleExample example = new SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test8() {
        SampleExample example = new SampleExample();


        int nr = App.random.nextInt(11);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test9() {
        SampleExample example = new SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test10() {
        SampleExample example = new SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }


    @Test
    public void alwaysFail() {
        Assert.assertTrue(false);
    }

}
