# Raportarea testelor automate folosind stack-ul ELK 

### 1. Introducere

Pentru acest atelier aveti nevoie de un calculator cu urmatoarele aplicatii instalate:

- java 7
- jmeter
- un client SSH 
  - pe UNIX se poate folosi cel nativ
  - pe Windows un exemplu este putty -> http://www.putty.org/

Pe UNIX, pentru a va conecta la masina de lucru rulati comanda urmatoare intr-un terminal:
```sh
$ ssh -i <key> ubuntu@<host>
```
### 2. [Elasticsearch], [Logstash], [Kibana]
**2.1. Elasticsearch**

- este un *search engine* bazat pe Apache Lucene
- este un *standalone database* server scris in Java ce stocheaza data intr-un mod sofisticat optimizat pentru *language based searches*
- este usor de lucrat cu el oferind un API HTTP/JSON
- scaleaza foarte usor oferind clustering 
- oferea *high-availability* prin replicarea datelor


**Exercitiul 1**

- Inspectati fisierul de configurare al serverului.Schimbati numele nodului. 
- Porniti serverul
- verificati conexiunea dintr-un browser
- accesati serverul prin plugin-ul "head"


```sh
# for starting the elasticsearch server
elasticsearch &
```
Elasticsearch ruleaza default pe portul **9200**, iar pentru a accesa interfata **head** trebuie sa va conectati la urmatorul URL:
```
<host>:9200/_plugin/head
```
**Exercitiul 2**

- Adaugati date in elastic search ruland *generate_load.sh*
```sh
cd /mnt/resources/tdt/elasticsearch
./generate_load.sh
```


**2.2.Logstash**

- *open-source data collection engine*
- poate colecta multe tipuri de loguri: log4j, Windows event logs, syslog etc.
- creaza un "pipe" intre sursa si destinatie
- poate procesa, filtra date

**2.3.Kibana**

- Platforma de analytics si vizualizare a datelor
- Ofera raportare in real-time

### 3. ELK stack in testare

**3.1.Introducere**

In cadrul acestui workshop vom incerca sa cream o platforma de raportare a testelor automate folosind tehnologiile prezentate anterior.

Cum va functiona?

- vom folosi jmeter ca si test runner, el va rula testele de performanta
- logurile de jmeter for fii scrise in real-time pe disk
- logstash va citi datele din aceste loguri si le va trimite in elasticsearch unde vor fii salvate
- kibana va interoga elasticsearch si va afisa datele cerute in diferite reprezentari (tabele, grafice)
- toate componentele de mai sus vor fii controlate printr-un job de Jenkins

**3.2.HTTP dummy server.Configurare/Rulare Jmeter**

**Exercitiul 3** - HTTP dummy server si Jmeter

- porniti HTTP dummy server

```sh
cd /mnt/http_app
nohup ./http_server.py &
```
Serverul va asculta pe portul **8000**.Verificati ca functioneaza accesandu-l dintr-un browser.Server-ul raspunde aleator cu **201** sau **500**.De asemenea el va asteapta un timp aleator ( 1 - 3 sec) inainte de a raspunde.

- rulati testul [dummyTest.jmx] din Jmeter folosind interfata grafica
- rulati acelasi test din linie de comanda 
```sh
jmeter -n -t /mnt/resources/tdt/jmeter_test/dummyTest.jmx -Jhost localhost -Joutput /mnt/results/jmeter.out
```
Inspectati rezultatele testului in /mnt/results/jmeter.out.

**3.3.Configurare logstash**

**Exercitiul 4** - configurare logstash

Deschideti fisierul [jmeter_v1.conf].Inspectati cele 3 sectiuni: input, filter si output.

Porniti logstash, apoi rulati testul.Verificati rezultatele in elasticsearch.
```sh
cd /mnt/logstash/
logstash -f /mnt/resources/tdt/logstash/jmeter_v1.conf
```

**Exercitiul 5** - maparea in elasticsearch

Deschideti fisierul [jmeter_v2.conf].Observati campurile adaugate aditional si conversia unora la tipul "integer".
Creati un [template de mapare] in elasticsearch pentru datele de tip *jmeter*.Adaugati template-ul in confgurarea serverului.
```
curl -XPUT localhost:9200/_template/jmeter -d '
...jmeter_template content ...
'
```
Porniti logstash cu jmeter_2.conf.
```sh
logstash -f /mnt/resources/tdt/logstash/jmeter_v2.conf
```
Vizualizati datele in elasticsearch.

**Exercitiul 6** - parametrizarea configuratiei de logstash

Vizualizati fisierul */mnt/resources/tdt/logstash/jmeter_final.conf*.
```
cat /mnt/resources/tdt/logstash/jmeter_final.conf.
```
Rulati [prepare_config.sh] cu urmatorii parametrii:
```sh
cd /mnt/resources/tdt
./logstash/prepate_config.sh -t dummyTest -b "24" -j dummyJob -p "/mnt/results/jmeter.out"
```
Vizualizati configuratia inca o data.

**Exercitiul 7** - Rularea din Jenkins

- creati un nou job de jenkins ce va rula testul dummyTest.jmx si va trimite rezultatele in elasticsearch
- job-ul va clona git repository-ul https://rogoz@bitbucket.org/rogoz/tdt.git
- va avea ca parametru numele testului e.g. dummyTest
- urmatorul shell script pentru pasul de build
```
mkdir -p ${WORKSPACE}/results
./logstash/prepate_config.sh -t ${test_name} -b ${BUILD_NUMBER} -j ${JOB_NAME} -p "${WORKSPACE}/results/jmeter.out"
cat logstash/jmeter_final.conf
logstash -f logstash/jmeter_final.conf &
sleep 15
jmeter -n -t /mnt/resources/tdt/jmeter_test/${test_name}.jmx -Jhost localhost -Joutput "${WORKSPACE}/results/jmeter.out"
```
- Rulati job-ul de 2-3 ori.

**Exercitiul 8**

Deschideti kibana
```
<host>:5601
```
Creati un dashboard cu urmatoarele grafice:

- 90 percentile latency / build number (bar chart)
- 90 percentile latency pentru FindProduct request/time (line chart)
- percentage of failures (response code 500) vs percentage of successful requests (rc 200)/request type (pie chart)
- number of successful/failed requests/user (bar chart)

**Exercitiul 9** - Bonus junit reporting

Raportati rezultatele testelor din aplicatia SampleApp in ELK stack. 







[dummyTest.jmx]:https://bitbucket.org/rogoz/tdt/raw/8d042d49df26c03f0be454581e3ed3eb41a281f6/jmeter_test/dummyTest.jmx
[jmeter_v1.conf]:https://bitbucket.org/rogoz/tdt/raw/8d042d49df26c03f0be454581e3ed3eb41a281f6/logstash/jmeter_v1.conf
[jmeter_v2.conf]:https://bitbucket.org/rogoz/tdt/raw/8d042d49df26c03f0be454581e3ed3eb41a281f6/logstash/jmeter_v2.conf
[template de mapare]:https://bitbucket.org/rogoz/tdt/raw/8d042d49df26c03f0be454581e3ed3eb41a281f6/elasticsearch/jmeter_template.json
[jmeter_final.conf]:https://bitbucket.org/rogoz/tdt/raw/8d042d49df26c03f0be454581e3ed3eb41a281f6/logstash/jmeter_final.conf
[prepare_config.sh]:https://bitbucket.org/rogoz/tdt/src/8d042d49df26c03f0be454581e3ed3eb41a281f6/logstash/prepate_config.sh?at=master&fileviewer=file-view-default
[Elasticsearch]:https://www.elastic.co/products/elasticsearch
[Logstash]:https://www.elastic.co/products/logstash
[Kibana]:https://www.elastic.co/products/kibana






