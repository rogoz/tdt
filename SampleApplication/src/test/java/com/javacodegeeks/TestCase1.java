package com.javacodegeeks;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by tuicu on 30/06/15.
 */
public class TestCase1 {

    @Test
    public void test1() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();

	
	int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test2() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test3() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();


        int nr = App.random.nextInt(3);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test4() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test5() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();


        int nr = App.random.nextInt(5);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test6() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();


        int nr = App.random.nextInt(7);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test7() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test8() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test9() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();


        int nr = App.random.nextInt(3);
        Assert.assertEquals(nr, 0);
    }

    @Test
    public void test10() {
        com.javacodegeeks.SampleExample example = new com.javacodegeeks.SampleExample();


        int nr = App.random.nextInt(2);
        Assert.assertEquals(nr, 0);
    }


    @Test
    public void alwaysFail() {
        Assert.assertTrue(false);
    }

}
